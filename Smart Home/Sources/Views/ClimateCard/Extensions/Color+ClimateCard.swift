//
//  Color+ClimateCard.swift
//  Smart Home
//
//  Created by Dmitry Roytman on 06.05.2022.
//

import SwiftUI

extension Color {
  static var cardBorderColor: Color { Color.white.opacity(0.1) }
}
