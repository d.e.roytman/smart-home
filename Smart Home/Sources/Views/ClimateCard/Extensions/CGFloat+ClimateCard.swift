//
//  CGFloat+ClimateCard.swift
//  Smart Home
//
//  Created by Dmitry Roytman on 06.05.2022.
//

import UIKit

extension CGFloat {
  static var cardCornerRadius: CGFloat { 22 }
  static var cardWidth: CGFloat { 144 }
  static var cardHeight: CGFloat { 164 }
  static var shadowRadius: CGFloat { 40 }
  static var shadowX: CGFloat { 0 }
  static var shadowY: CGFloat { 20 }
  
  static var cardContentPaddingVertical: CGFloat { 20 }
  static var cardContentPaddingHorizontal: CGFloat { 10 }
}
