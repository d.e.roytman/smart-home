//
//  CircleIcon.swift
//  Smart Home
//
//  Created by Dmitry Roytman on 06.05.2022.
//

import SwiftUI

struct CircleIcon: View {
  let iconName: String
  var body: some View {
    Image(systemName: iconName)
      .font(.title2.weight(.semibold))
      .foregroundColor(.white)
      .frame(width: .iconDiameter, height: .iconDiameter)
      .background(LinearGradient.background)
      .clipShape(Circle())
  }
}

struct CircleIcon_Previews: PreviewProvider {
  static var previews: some View {
    VStack(spacing: 10) {
      ClimateCard(state: .preview)
      CircleIcon(iconName: "humidity.fill")
    }
  }
}

fileprivate extension CGFloat {
  static var iconDiameter: CGFloat { 60 }
}

private extension LinearGradient {
  static var background: LinearGradient {
    LinearGradient(
      [.temperatureRing1, .temperatureRing2],
      startPoint: .top,
      endPoint: .bottom
    )
  }
}
