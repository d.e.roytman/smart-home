//
//  ShadowModifier.swift
//  Smart Home
//
//  Created by Dmitry Roytman on 06.05.2022.
//

import SwiftUI

struct ShadowModifier: ViewModifier {
  func body(content: Content) -> some View {
    content
      .shadow(
        color: .cardShadow,
        radius: .shadowRadius,
        x: .shadowX,
        y: .shadowY
      )
  }
}
