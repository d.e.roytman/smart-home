//
//  ClimateCard.swift
//  Smart Home
//
//  Created by Dmitry Roytman on 06.05.2022.
//

import SwiftUI

struct ClimateCardState: Equatable {
  var iconName: String
  var index: String
  var measure: String
}

struct ClimateCard: View {
  var state: ClimateCardState
  
  var body: some View {
    ZStack {
      // MARK: - Card
      RoundedRectangle(
        cornerRadius: .cardCornerRadius,
        style: .continuous
      )
      .fill(Color.cardBackground)
      .modifier(ShadowModifier())
      .overlay(CardBorder())
      VStack(spacing: .outerVStackSpacing) {
        CircleIcon(iconName: state.iconName)
        
        VStack(spacing: .innerVStackSpacing) {
          Text(state.index)
            .font(.headline)
            .foregroundColor(.white)
          
          Text(state.measure)
            .font(.title3)
            .fontWeight(.semibold)
            .foregroundColor(.white)
            .opacity(0.6)
        }
      }
      .padding(.vertical, .cardContentPaddingVertical)
      .padding(.horizontal, .cardContentPaddingHorizontal)
    }
    .frame(width: .cardWidth, height: .cardHeight)
  }
}

struct ClimateCard_Previews: PreviewProvider {
  static var previews: some View {
    ClimateCard(state: .preview)
      .frame(maxWidth: .infinity, maxHeight: .infinity)
      .background(Color.background)
  }
}

// MARK: Helpers

#if DEBUG
extension ClimateCardState {
  static var preview: ClimateCardState {
    .init(
      iconName: "humidity.fill",
      index: "Inside Humidity",
      measure: "50%"
    )
  }
}
#endif

extension CGFloat {
  fileprivate static var innerVStackSpacing: CGFloat { 8 }
  fileprivate static var outerVStackSpacing: CGFloat { 10 }
}
