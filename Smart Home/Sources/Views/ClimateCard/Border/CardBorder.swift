//
//  CardBorder.swift
//  Smart Home
//
//  Created by Dmitry Roytman on 06.05.2022.
//

import SwiftUI

struct CardBorder: View {
  var body: some View {
    RoundedRectangle(
      cornerRadius: .cardCornerRadius,
      style: .continuous
    )
    .stroke(Color.cardBorderColor, lineWidth: 1)
  }
}

struct CardBorder_Previews: PreviewProvider {
  static var previews: some View {
    CardBorder()
  }
}

