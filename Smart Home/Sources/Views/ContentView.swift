//
//  ContentView.swift
//  Smart Home
//
//  Created by Dmitry Roytman on 06.05.2022.
//

import SwiftUI

struct ContentView: View {
  var body: some View {
    NavigationView {
      ZStack {
        Color.background
          .ignoresSafeArea()
        
        ScrollView {
          VStack(spacing: .zero) {
            ThermometerView(
              thermometerViewModel: ThermometerViewModel(
                smartHomeService: SmartHomeServiceMock()
              )
            )
              .padding(.top, .paddingTop)
              .padding(.bottom, .paddingBottom)
            HStack(spacing: .climateCardSpacing) {
              ClimateCard(state: .humidity)
              ClimateCard(state: .outside)
            }
          }
        }
      }
      .navigationTitle("Thermostat")
      .navigationBarTitleDisplayMode(.inline)
    }
    .navigationViewStyle(.stack)
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
      .preferredColorScheme(.dark)
  }
}

// MARK: - Helper

extension CGFloat {
  fileprivate static var paddingTop: CGFloat { 30 }
  fileprivate static var paddingBottom: CGFloat { 60 }
  fileprivate static var climateCardSpacing: CGFloat { 20 }
}

extension ClimateCardState {
  fileprivate static var humidity: ClimateCardState {
    .init(iconName: "humidity.fill", index: "Inside humidity", measure: "49%")
  }
  fileprivate static var outside: ClimateCardState {
    .init(iconName: "thermometer", index: "Outside temp.", measure: "-10°")
  }
}
