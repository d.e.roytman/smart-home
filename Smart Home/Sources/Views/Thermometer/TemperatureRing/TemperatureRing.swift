//
//  TemperatureRing.swift
//  Smart Home
//
//  Created by Dmitry Roytman on 06.05.2022.
//

import SwiftUI

struct TemperatureRing: View {
  var ringValue: CGFloat
  var body: some View {
    Circle()
      .inset(by: .ringInset)
      .trim(from: .ringTrimFrom, to: .ringTrimTo(ringValue))
      .stroke(LinearGradient.temperatureRing, style: .temperatureRing)
      .frame(width: .ringSize, height: .ringSize)
      .rotationEffect(.rotation)
      .animation(.ring, value: ringValue)
  }
}

// MARK: - Preview

struct TemperatureRing_Previews: PreviewProvider {
  static var previews: some View {
    TemperatureRing(ringValue: 0.5)
  }
}

// MARK: - Helpers

extension CGFloat {
  fileprivate static var ringSize: CGFloat { 220 }
  fileprivate static var ringInset: CGFloat { 5 }
  fileprivate static var ringTrimFrom: CGFloat { 0.099 }
  fileprivate static func ringTrimTo(_ value: CGFloat) -> CGFloat {
    Swift.min(value, 0.75)
  }
  fileprivate static var lineWidth: CGFloat { 10 }
}

extension Double {
  fileprivate static var rotation: Double { 90 }
}

extension Angle {
  fileprivate static var rotation: Angle { .degrees(.rotation) }
}

extension Animation {
  fileprivate static var ring: Animation {
    .linear(duration: 1)
  }
}

extension LinearGradient {
  fileprivate static var temperatureRing: LinearGradient {
    .init([.temperatureRing1, .temperatureRing2])
  }
}

extension StrokeStyle {
  fileprivate static var temperatureRing: StrokeStyle {
    .init(lineWidth: .lineWidth, lineCap: .round, lineJoin: .round)
  }
}
