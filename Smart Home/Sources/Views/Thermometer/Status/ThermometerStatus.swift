//
//  ThermometerStatus.swift
//  Smart Home
//
//  Created by Dmitry Roytman on 06/05/2022.
//

import Foundation

enum ThermometerStatus: String {
  case heating
  case cooling
  case reaching
}

extension ThermometerStatus: CustomStringConvertible {
  var description: String {
    rawValue.uppercased()
  }
}
