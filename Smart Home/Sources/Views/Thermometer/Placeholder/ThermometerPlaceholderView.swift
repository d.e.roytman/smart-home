//
//  ThermometerPlaceholderView.swift
//  Smart Home
//
//  Created by Dmitry Roytman on 06.05.2022.
//

import SwiftUI

struct ThermometerPlaceholderView: View {
  var body: some View {
    Circle()
      .fill(LinearGradient.placeholderFill)
      .frame(width: .placeholderSize, height: .placeholderSize)
      .placeholderShadow()
      .overlay(PlaceholderBorder())
      .overlay(PlaceholderInnerShadow())
  }
}

struct ThermometerPlaceholderView_Previews: PreviewProvider {
  static var previews: some View {
    ThermometerPlaceholderView()
      .frame(maxWidth: .infinity, maxHeight: .infinity)
      .background(Color.background)
  }
}

// MARK: - Helpers

extension View {
  fileprivate func placeholderShadow() -> some View {
    modifier(ThermometerPlaceholderShadowModifier())
  }
}

extension CGFloat {
  fileprivate static var placeholderSize: CGFloat { 244 }
  fileprivate static var outerVStackSpacing: CGFloat { 10 }
}

extension LinearGradient {
  fileprivate static var placeholderFill: LinearGradient {
    .init([.placeholder1, .placeholder2])
  }
}
