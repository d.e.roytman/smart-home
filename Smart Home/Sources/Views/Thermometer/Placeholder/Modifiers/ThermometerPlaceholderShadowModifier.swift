//
//  ThermometerPlaceholderShadowModifier.swift
//  Smart Home
//
//  Created by Dmitry Roytman on 06.05.2022.
//

import SwiftUI

struct ThermometerPlaceholderShadowModifier: ViewModifier {
  func body(content: Content) -> some View {
    content
      .shadow(
        color: .placeholderDropShadow,
        radius: .placeholderShadowRadius,
        x: .placeholderShadowX,
        y: .placeholderShadowY
      )
  }
}

extension CGFloat {
  fileprivate static var placeholderShadowRadius: CGFloat { 30 }
  fileprivate static var placeholderShadowX: CGFloat { 0 }
  fileprivate static var placeholderShadowY: CGFloat { 15 }
}
