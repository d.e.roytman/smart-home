//
//  PlaceholderBorder.swift
//  Smart Home
//
//  Created by Dmitry Roytman on 06.05.2022.
//

import SwiftUI

struct PlaceholderBorder: View {
  var body: some View {
    Circle()
      .stroke(LinearGradient.placeholderBorder, lineWidth: 0.8)
  }
}

struct PlaceholderBorder_Previews: PreviewProvider {
  static var previews: some View {
    PlaceholderBorder()
  }
}

// MARK: - Helper

extension LinearGradient {
  fileprivate static var placeholderBorder: LinearGradient {
    .init(
      [
        .black.opacity(0.36),
        .white.opacity(0.11)
      ]
    )
  }
}
