//
//  PlaceholderInnerShadow.swift
//  Smart Home
//
//  Created by Dmitry Roytman on 06.05.2022.
//

import SwiftUI

struct PlaceholderInnerShadow: View {
  var body: some View {
    Circle()
      .stroke(Color.placeholderInnerShadow, lineWidth: .strokeLineWidth)
      .blur(radius: .blurRadius)
      .offset(x: .innerShadowOffsetX, y: .innerShadowOffsetY)
      .mask {
        Circle()
          .fill(LinearGradient.placeholderInnerShadow)
      }
  }
}

struct PlaceholderInnerShadow_Previews: PreviewProvider {
  static var previews: some View {
    PlaceholderInnerShadow()
  }
}

// MARK: - Helpers

extension LinearGradient {
  fileprivate static var placeholderInnerShadow: LinearGradient {
    LinearGradient([.black, .clear], startPoint: .top, endPoint: .bottom)
  }
}

extension CGFloat {
  fileprivate static var blurRadius: CGFloat { 7 }
  fileprivate static var innerShadowOffsetX: CGFloat { 0 }
  fileprivate static var innerShadowOffsetY: CGFloat { 3 }
  fileprivate static var strokeLineWidth: CGFloat { 2 }
}
