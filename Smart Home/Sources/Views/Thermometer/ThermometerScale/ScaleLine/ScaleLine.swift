//
//  ScaleLine.swift
//  Smart Home
//
//  Created by Dmitry Roytman on 06/05/2022.
//

import SwiftUI

struct ScaleLine: View {
  let line: Int
  
  var body: some View {
    VStack {
      Rectangle()
        .fill(Color.scaleLine)
      .frame(width: .lineWidth, height: .lineHeight)
      Spacer()
    }
    .rotationEffect(.degrees(9 * Double(line)))
    .rotationEffect(.degrees(-90))
  }
}

struct ScaleLine_Previews: PreviewProvider {
  static var previews: some View {
    ZStack {
      ForEach(0...21, id: \.self) { line in
        ScaleLine(line: line)
      }
      .frame(width: 200, height: 200)
    }
  }
}

// MARK: - Helper

extension CGFloat {
  fileprivate static var lineWidth: CGFloat { 2 }
  fileprivate static var lineHeight: CGFloat { 10 }
}
