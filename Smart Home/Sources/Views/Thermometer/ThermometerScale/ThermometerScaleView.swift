//
//  ThermometerScaleView.swift
//  Smart Home
//
//  Created by Dmitry Roytman on 06/05/2022.
//

import SwiftUI

struct ThermometerScaleView: View {
  var body: some View {
    ZStack {
      VStack(spacing: .horizontalPadding) {
        ScaleLabel(temperature: 20)
        HStack(spacing: .verticalPadding) {
          ScaleLabel(temperature: 10)
          ScaleView()
          ScaleLabel(temperature: 30)
        }
      }
      .padding(.bottom, .bottomPadding)
    }
  }
}

struct ThermometerScaleView_Previews: PreviewProvider {
  static var previews: some View {
    ThermometerView(thermometerViewModel: .preview)
      .frame(maxWidth: .infinity, maxHeight: .infinity)
      .background(Color.background)
  }
}

// MARK: - Helper

extension CGFloat {
  fileprivate static var horizontalPadding: CGFloat { 16 }
  fileprivate static var verticalPadding: CGFloat { 16 }
  fileprivate static var bottomPadding: CGFloat { 35 }
}

// MARK: - Views

private struct ScaleLabel: View {
  let temperature: Int
  
  var body: some View {
    Text("\(temperature)°")
      .font(.subheadline)
      .foregroundColor(.white.opacity(0.3))
  }
}
