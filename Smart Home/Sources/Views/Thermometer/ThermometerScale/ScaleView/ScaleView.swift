//
//  ScaleView.swift
//  Smart Home
//
//  Created by Dmitry Roytman on 06/05/2022.
//

import SwiftUI

struct ScaleView: View {
  var body: some View {
    ZStack {
      ForEach(0..<21) { line in
        ScaleLine(line: line)
      }
      .frame(width: .scaleSize, height: .scaleSize)
    }
  }
}

struct ScaleView_Previews: PreviewProvider {
  static var previews: some View {
    ScaleView()
  }
}

// MARK: - Helper

extension CGFloat {
  fileprivate static var scaleSize: CGFloat { 276 }
}
