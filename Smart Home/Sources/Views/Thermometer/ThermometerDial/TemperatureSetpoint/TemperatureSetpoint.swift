//
//  TemperatureSetpoint.swift
//  Smart Home
//
//  Created by Dmitry Roytman on 06/05/2022.
//

import SwiftUI

struct TemperatureSetpoint: View {
  @Binding var degrees: CGFloat
  var body: some View {
    Circle()
      .fill(LinearGradient.temperatureSetpoint)
      .frame(width: .setpointSize, height: .setpointSize)
      .frame(width: .innerDialSize, height: .innerDialSize, alignment: .top)
      .offset(x: .zero, y: .offsetY)
      .rotationEffect(.degrees(degrees + .halfCircle))
      .animation(.temperatureSetpoint, value: degrees)
  }
}

struct TemperatureSetpoint_Previews: PreviewProvider {
  static var previews: some View {
    TemperatureSetpoint(degrees: .constant(0))
  }
}

// MARK: - Helpers

extension CGFloat {
  fileprivate static var setpointSize: CGFloat { 15 }
  fileprivate static var offsetY: CGFloat { 7.5 }
}


extension Double {
  fileprivate static var halfCircle: Double { 180 }
}
extension LinearGradient {
  fileprivate static var temperatureSetpoint: LinearGradient {
    LinearGradient([.temperatureSetpoint1, .temperatureSetpoint2])
  }
}
extension Animation {
  fileprivate static var temperatureSetpoint: Animation {
    .easeInOut(duration: 1)
  }
}
