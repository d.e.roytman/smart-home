//
//  OuterDialInnerShadow.swift
//  Smart Home
//
//  Created by Dmitry Roytman on 06/05/2022.
//

import SwiftUI

struct OuterDialInnerShadow: View {
  var body: some View {
    Circle()
      .stroke(Color.stroke, lineWidth: .lineWidth)
      .blur(radius: .blurRadius)
      .offset(x: .offset, y: .offset)
      .mask {
        Circle()
          .fill(LinearGradient([.black, .clear]))
      }
  }
}

struct OuterDialInnerShadow_Previews: PreviewProvider {
  static var previews: some View {
    OuterDialInnerShadow()
  }
}

// MARK: - Helpers

extension Color {
  fileprivate static var stroke: Color { .white.opacity(0.1) }
}

extension CGFloat {
  fileprivate static var lineWidth: CGFloat { 4 }
  fileprivate static var blurRadius: CGFloat { 8 }
  fileprivate static var offset: CGFloat { 3 }
}
