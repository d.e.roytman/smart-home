//
//  OuterDialShadowModifier.swift
//  Smart Home
//
//  Created by Dmitry Roytman on 06/05/2022.
//

import SwiftUI

struct OuterDialShadowModifier: ViewModifier {
  func body(content: Content) -> some View {
    content
      .shadow(color: .shadow, radius: .innerShadowRadius, x: .zero, y: .innerShadowY)
      .shadow(color: .shadow, radius: .outerShadowRadius, x: .zero, y: .outerShadowY)
  }
}

// MARK: - Helpers

extension CGFloat {
  fileprivate static var innerShadowRadius: CGFloat { 60 }
  fileprivate static var outerShadowRadius: CGFloat { 16 }
  fileprivate static var innerShadowY: CGFloat { 30 }
  fileprivate static var outerShadowY: CGFloat { 8 }
}

extension Color {
  fileprivate static var shadow: Color { .black.opacity(0.2) }
}
