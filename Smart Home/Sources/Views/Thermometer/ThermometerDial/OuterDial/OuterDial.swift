//
//  OuterDial.swift
//  Smart Home
//
//  Created by Dmitry Roytman on 06/05/2022.
//

import SwiftUI

struct OuterDial: View {
    var body: some View {
        Circle()
        .fill(LinearGradient.outerDial)
        .frame(width: .outerDialSize, height: .outerDialSize)
        .shadow()
        .overlay(OuterDialBorder())
        .overlay(OuterDialInnerShadow())
    }
}

struct OuterDial_Previews: PreviewProvider {
  static var previews: some View {
    ThermometerDialView(degrees: .constant(36))
      .frame(maxWidth: .infinity, maxHeight: .infinity)
      .background(Color("Background"))
  }
}

// MARK: - Helpers

extension View {
  fileprivate func shadow() -> some View {
    modifier(OuterDialShadowModifier())
  }
}

extension LinearGradient {
  fileprivate static var outerDial: LinearGradient {
    LinearGradient([.outerDial1, .outerDial2])
  }
}

extension CGFloat {
  static var outerDialSize: CGFloat { 200 }
}
