//
//  OuterDialBorder.swift
//  Smart Home
//
//  Created by Dmitry Roytman on 06/05/2022.
//

import SwiftUI

struct OuterDialBorder: View {
  var body: some View {
    Circle()
      .stroke(LinearGradient.overlay, lineWidth: .lineWidth)
  }
}


struct OuterDialBorder_Previews: PreviewProvider {
  static var previews: some View {
    OuterDialBorder()
  }
}

extension LinearGradient {
  fileprivate static var overlay: LinearGradient {
    LinearGradient([.white.opacity(0.2), .black.opacity(0.19)])
  }
}

extension CGFloat {
  fileprivate static var lineWidth: CGFloat { 1 }
}
