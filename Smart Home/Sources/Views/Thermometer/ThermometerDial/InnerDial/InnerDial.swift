//
//  InnerDial.swift
//  Smart Home
//
//  Created by Dmitry Roytman on 06/05/2022.
//

import SwiftUI

struct InnerDial: View {
  var body: some View {
    Circle()
      .fill(LinearGradient.innerDial)
      .frame(width: .innerDialSize, height: .innerDialSize)
  }
}

struct InnerDial_Previews: PreviewProvider {
  static var previews: some View {
    InnerDial()
  }
}

// MARK: - Helpers

extension LinearGradient {
  fileprivate static var innerDial: LinearGradient {
    LinearGradient([.innerDial1, .innerDial2])
  }
}

extension CGFloat {
  static var innerDialSize: CGFloat { 172 }
}

