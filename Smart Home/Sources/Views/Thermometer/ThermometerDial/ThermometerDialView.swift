//
//  ThermometerDialView.swift
//  Smart Home
//
//  Created by Dmitry Roytman on 06/05/2022.
//

import SwiftUI

struct ThermometerDialView: View {
  @Binding var degrees: CGFloat
  
  var body: some View {
    ZStack {
      OuterDial()
      InnerDial()
      TemperatureSetpoint(degrees: $degrees)
    }
  }
}

// MARK: - Preview

struct ThermometerDialView_Previews: PreviewProvider {
  static var previews: some View {
    ThermometerDialView(degrees: .constant(7))
      .frame(maxWidth: .infinity, maxHeight: .infinity)
      .background(Color("Background"))
  }
}
