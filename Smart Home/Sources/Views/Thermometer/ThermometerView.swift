//
//  ThermometerView.swift
//  Smart Home
//
//  Created by Dmitry Roytman on 06.05.2022.
//

import SwiftUI

struct ThermometerView: View {
  @ObservedObject var thermometerViewModel: ThermometerViewModel
  
  var body: some View {
    ZStack {
      ThermometerScaleView()
      ThermometerPlaceholderView()
      TemperatureRing(ringValue: thermometerViewModel.ringValue)
      ThermometerDialView(degrees: $thermometerViewModel.degrees)
        .onDrag { thermometerViewModel.onChanged($0) }
      ThermometerSummaryView(thermometerViewModel:  thermometerViewModel)
    }
    .onAppear {
      thermometerViewModel.onAppear()
    }
  }
}

// MARK: - Preview

struct ThermometerView_Previews: PreviewProvider {
  static var previews: some View {
    ThermometerView(thermometerViewModel: .preview)
      .frame(maxWidth: .infinity, maxHeight: .infinity)
      .background(Color.background)
  }
}


// MARK: - Helper

extension View {
  fileprivate func onDrag(_ closure: @escaping (DragGesture.Value) -> Void) -> some View {
    gesture(
      DragGesture()
        .onChanged(closure)
    )
  }
}
