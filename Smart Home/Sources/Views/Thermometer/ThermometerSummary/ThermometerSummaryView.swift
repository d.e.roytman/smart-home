//
//  ThermometerSummaryView.swift
//  Smart Home
//
//  Created by Dmitry Roytman on 06/05/2022.
//

import SwiftUI

struct ThermometerSummaryView: View {
  @ObservedObject var thermometerViewModel: ThermometerViewModel
  
  var body: some View {
    VStack(spacing: .zero) {
      Text(thermometerViewModel.status.description)
        .font(.headline)
        .foregroundColor(.white)
        .opacity(thermometerViewModel.shouldShowStatus ? .opacity : .zero)
        .animation(.status, value: thermometerViewModel.shouldShowStatus)
      
      Text("\(thermometerViewModel.currentTemperature, specifier: "%.0f")")
          .font(.header)
          .foregroundColor(.white)
      
      Image(systemName: "leaf.fill")
        .font(.title2.bold())
        .foregroundColor(.green)
    }
    .padding(.top, .padding)
  }
}

struct ThermometerSummaryView_Previews: PreviewProvider {
  static var previews: some View {
    ThermometerSummaryView(thermometerViewModel: .preview)
    .background(Color.innerDial2)
  }
}

// MARK: - Helper

extension CGFloat {
  fileprivate static var padding: CGFloat { 40 }
}

extension Double {
  fileprivate static var opacity: Double { 0.6 }
  fileprivate static var duration: Double { 0.5 }
}

extension Animation {
  fileprivate static var status: Animation {
    .easeIn(duration: .duration)
  }
}

extension Font {
  fileprivate static var header: Font {
    .system(size: 54)
  }
}
