//
//  EventBusServiceProtocol.swift
//  Smart Home
//
//  Created by Dmitry Roytman on 08/05/2022.
//

import Foundation
import Combine

protocol EventBusServiceProtocol: AnyObject {
  typealias Event = Date
  var eventPassthroughSubject: PassthroughSubject<Date, Never> { get }
}
