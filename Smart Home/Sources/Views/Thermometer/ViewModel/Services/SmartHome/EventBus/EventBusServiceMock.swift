//
//  EventBusServiceMock.swift
//  Smart Home
//
//  Created by Dmitry Roytman on 08/05/2022.
//

import Foundation
import Combine

final class EventBusServiceMock: EventBusServiceProtocol {
  
  // MARK: - Internal types
  
  typealias Event = EventBusServiceProtocol.Event
  
  // MARK: - Internal properties
  
  let eventPassthroughSubject = PassthroughSubject<Event, Never>()
  
  // MARK: - Private properties
  
  private let timer: AnyPublisher<Event, Never>

  private var cancelables: Set<AnyCancellable> = []
  
  // MARK: - Init
  
  init(timer: Timer.TimerPublisher = .everySecond) {
    self.timer = timer.autoconnect().eraseToAnyPublisher()
    bind()
  }
  
  // MARK: - Private methods
  
  // MARK: - Binding
  
  private func bind() {
    timer
      .sink { [weak self] newEvent in
        guard let self = self else {
          assertionFailure("Self is not expected to be nil")
          return
        }
        self.eventPassthroughSubject.send(newEvent)
      }
      .store(in: &cancelables)
  }
  
}

extension Timer.TimerPublisher {
  fileprivate static var everySecond: Timer.TimerPublisher {
    Timer
      .publish(every: 1, on: .main, in: .common)
  }
}
