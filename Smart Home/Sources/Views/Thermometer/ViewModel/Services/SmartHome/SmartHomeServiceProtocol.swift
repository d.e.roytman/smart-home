//
//  SmartHomeServiceProtocol.swift
//  Smart Home
//
//  Created by Dmitry Roytman on 08/05/2022.
//

import Foundation
import Combine

protocol SmartHomeServiceProtocol {
  var currentTemperature: CurrentValueSubject<Int, Never> { get }
  var thermometerStatus: CurrentValueSubject<ThermometerStatus, Never> { get }
  var targetTemperature: Int { get }
  
  func setTargetTemperature(_ targetTemperature: Int)
  func start()
  func stop()
}
