//
//  SmartHomeMockService.swift
//  Smart Home
//
//  Created by Dmitry Roytman on 08/05/2022.
//

import SwiftUI
import Combine

final class SmartHomeServiceMock: SmartHomeServiceProtocol {
  
  // MARK: - Internal properties
  
  private(set) var currentTemperature = CurrentValueSubject<Int, Never>(.zero)
  private(set) var thermometerStatus = CurrentValueSubject<ThermometerStatus, Never>(.cooling)
  private(set) var targetTemperature: Int = .zero
  
  // MARK: - Private properties
  
  /// These PassthroughSubject's might seem to be redundant in that particular case. But from my perspective, it's a good pattern that allows decoupling events emitter(-s) with events consumer(-s)
  private let temperatureAdjustmentSubject = PassthroughSubject<Int, Never>()
  private let thermometerStatusSubject = PassthroughSubject<ThermometerStatus, Never>()
  
  private let eventBus: EventBusServiceProtocol
  private var cancelables: Set<AnyCancellable> = []
  
  // MARK: - Init
  
  init(eventBus: EventBusServiceProtocol = EventBusServiceMock()) {
    self.eventBus = eventBus
    bind()
  }
  
  // MARK: - Internal methods
  
  func setTargetTemperature(_ targetTemperature: Int) {
    self.targetTemperature = targetTemperature
  }
  
  func start() {
    // Send some mocked initial values
    targetTemperature = .initialTemperature
    Just<Int>(.initialTemperature)
      .sink { [weak currentTemperature] in
        currentTemperature?.send($0)
      }
      .store(in: &cancelables)
    Just<ThermometerStatus>(.reaching)
      .sink { [weak thermometerStatus] in
        thermometerStatus?.send($0)
      }
      .store(in: &cancelables)
  }
  
  func stop() {
    // As far as it's a mock - do nothing here
  }
  
  // MARK: - Private methods
  
  // MARK: - Binding
  
  private func bind() {
    let eventPublisher = eventBus.eventPassthroughSubject
      .map { _ -> Void in Void() }
      .receive(on: DispatchQueue.main)
      .eraseToAnyPublisher()
      .share()
    
    eventPublisher
      .map { [weak self, weak currentTemperature] _ -> Int in
        guard let self = self else {
          assertionFailure("Self is not expected to be nil")
          return .zero
        }
        guard let currentTemperature = currentTemperature?.value else {
          assertionFailure("currentTemperature is not expected to be nil")
          return .zero
        }
        if currentTemperature < self.targetTemperature {
          return 1
        } else if currentTemperature > self.targetTemperature {
          return -1
        } else {
          return 0
        }
      }
      .sink { [weak temperatureAdjustmentSubject] in
        temperatureAdjustmentSubject?.send($0)
      }
      .store(in: &cancelables)
    
    temperatureAdjustmentSubject
      .sink { [weak currentTemperature] in
        guard let currentTemperature = currentTemperature else {
          assertionFailure("currentTemperature is not expected to be nil")
          return
        }
        currentTemperature.value += $0
      }
      .store(in: &cancelables)
    
    eventPublisher.map { [weak self, weak currentTemperature] _ -> ThermometerStatus in
      guard let self = self else {
        assertionFailure("Self is not expected to be nil")
        return .reaching
      }
      guard let currentTemperature = currentTemperature?.value else {
        assertionFailure("currentTemperature is not expected to be nil")
        return .reaching
      }
      
      if self.targetTemperature < currentTemperature {
        return .cooling
      } else if self.targetTemperature > currentTemperature {
        return .heating
      } else {
        return .reaching
      }
    }
    .sink { [weak thermometerStatusSubject] newValue in
      guard let thermometerStatusSubject = thermometerStatusSubject else {
        assertionFailure("thermometerStatus is not expected to be nil")
        return
      }
      thermometerStatusSubject.send(newValue)
    }
    .store(in: &cancelables)
    
    thermometerStatusSubject.sink { [weak thermometerStatus] newValue in
      guard let thermometerStatus = thermometerStatus else {
        assertionFailure("thermometerStatus is not expected to be nil")
        return
      }
      thermometerStatus.send(newValue)
    }
    .store(in: &cancelables)
  }
}

// MARK: - Constants

extension Int {
  fileprivate static var initialTemperature: Int { 14 }
}
