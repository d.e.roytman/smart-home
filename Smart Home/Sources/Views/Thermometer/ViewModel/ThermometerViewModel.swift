//
//  ThermometerViewModel.swift
//  Smart Home
//
//  Created by Dmitry Roytman on 08/05/2022.
//

import SwiftUI
import Combine

final class ThermometerViewModel: ObservableObject {
  
  // MARK: - Internal properties
  
  var ringValue: CGFloat { currentTemperature / .steps }
  
  // MARK: - Published properties
  
  @Published private(set) var currentTemperature: CGFloat = 0
  @Published var degrees: CGFloat = .minDegrees {
    didSet {
      setTargetTemperature()
    }
  }
  @Published private(set) var status: ThermometerStatus = .reaching
  @Published private(set) var shouldShowStatus = true
  
  // MARK: - Private properties
  
  private let smartHomeService: SmartHomeServiceProtocol
  
  // MARK: - Init
  
  init(smartHomeService: SmartHomeServiceProtocol) {
    self.smartHomeService = smartHomeService
    currentTemperature = CGFloat(smartHomeService.currentTemperature.value)
    status = smartHomeService.thermometerStatus.value
    bind()
  }
  
  // MARK: - Internal methods
  
  func onAppear() {
    smartHomeService.start()
    
    smartHomeService.currentTemperature
      .share()
      .first()
      .map(CGFloat.init)
      .map { $0 * .step }
      .assign(to: &$degrees)
  }
  
  func onChanged(_ value: DragGesture.Value) {
    let x = min(max(value.location.x, .zero), .outerDialSize)
    let y = min(max(value.location.y, .zero), .outerDialSize)
    let endPoint = CGPoint(x: x, y: y)
    
    let angle = calculateAngle(endPoint: endPoint)
    
    guard angle >= .minDegrees || angle <= .maxDegrees else { return }
    degrees = angle.ceiled()
  }
  
  // MARK: - Private methods
  
  private func bind() {
    smartHomeService.currentTemperature
      .map(CGFloat.init)
      .assign(to: &$currentTemperature)

    let sharedThermometerStatus = smartHomeService.thermometerStatus
      .share()
    
    sharedThermometerStatus
      .assign(to: &$status)
    
    sharedThermometerStatus
      .map { $0 != .reaching }
      .assign(to: &$shouldShowStatus)
  }
  
  private func calculateAngle(endPoint: CGPoint) -> CGFloat {
    let centerPoint: CGPoint = .centerPoint
    let radians = atan2(endPoint.x - centerPoint.x, centerPoint.y - endPoint.y)
    let degrees = .halfOfArc + (radians * .halfOfArc / .pi)
    
    return degrees
  }
  
  private func setTargetTemperature() {
    let targetTemperature = Int(degrees.targetTemperature())
    smartHomeService.setTargetTemperature(targetTemperature)
  }
}

// MARK: - Constants

extension CGFloat {
  fileprivate func ceiled() -> CGFloat {
    self - remainder(dividingBy: .step)
  }
  
  fileprivate func targetTemperature() -> CGFloat {
    return Swift.min(Swift.max(self / .step, .minTemperature), .maxTemperature)
  }
  
  fileprivate static var minTemperature: CGFloat { 4 }
  fileprivate static var maxTemperature: CGFloat { 30 }
}

extension CGFloat {
  fileprivate static var minDegrees: CGFloat { minTemperature * step }
  fileprivate static var maxDegrees: CGFloat { maxTemperature * step }
}

extension CGFloat {
  fileprivate static var step: CGFloat { arc / steps }
  fileprivate static var steps: CGFloat { 40 }
  fileprivate static var halfOfArc: CGFloat { arc / 2 }
  private static var arc: CGFloat { 360 }
}

extension CGPoint {
  fileprivate static var centerPoint: CGPoint {
    CGPoint(x: .outerDialSize / 2, y: .outerDialSize / 2)
  }
}

#if DEBUG
// MARK: - Preview

extension ThermometerViewModel {
  static var preview: ThermometerViewModel {
    .init(smartHomeService: SmartHomeServiceMock())
  }
}
#endif
