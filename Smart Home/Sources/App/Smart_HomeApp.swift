//
//  Smart_HomeApp.swift
//  Smart Home
//
//  Created by Dmitry Roytman on 06.05.2022.
//

import SwiftUI

@main
struct Smart_HomeApp: App {
  var body: some Scene {
    WindowGroup {
      ContentView()
    }
  }
}
