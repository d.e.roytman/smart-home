import SwiftUI

// MARK: - Asset Catalogs

extension Color {
  static var background: Color { Color("Background") }
  static var cardBackground: Color { Color("Card Background") }
  static var cardShadow: Color { Color("Card Shadow") }
  static var innerDial1: Color { Color("Inner Dial 1") }
  static var innerDial2: Color { Color("Inner Dial 2") }
  static var outerDial1: Color { Color("Outer Dial 1") }
  static var outerDial2: Color { Color("Outer Dial 2") }
  static var placeholder1: Color { Color("Placeholder 1") }
  static var placeholder2: Color { Color("Placeholder 2") }
  static var placeholderDropShadow: Color { Color("Placeholder Drop Shadow") }
  static var placeholderInnerShadow: Color { Color("Placeholder Inner Shadow") }
  static var scaleLine: Color { Color("Scale Line") }
  static var temperatureRing1: Color { Color("Temperature Ring 1") }
  static var temperatureRing2: Color { Color("Temperature Ring 2") }
  static var temperatureSetpoint1: Color { Color("Temperature Setpoint 1") }
  static var temperatureSetpoint2: Color { Color("Temperature Setpoint 2") }
}
